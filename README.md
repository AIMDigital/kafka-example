# Spring Boot with Kafka and React

## Description

This project serves as a quick prototype and foundation to demonstrate how to integrate Java Spring Boot and ReactJs with Apache Kafka.

The Spring Boot application exposes a POST endpoint to which messages can posted and consequently forwarded onto Kafka.

That same Spring Boot application then listens for messages on Kafka and exposes those via a WebSocket (STOMP) endpoint.

The ReactJs app consumes the Spring Boot's WebSocket endpoint and displays the messages in the browser.

![Kafka example](demo.gif)

## How to run

### Prerequisites
Java 11, Maven, NPM, NodeJS and Docker (optional)

### Via Docker
You can run the ZooKeeper, Kafka, Spring Boot and ReactJs applications all via Docker Compose.

```bash
docker-compose build
docker-compose up -d
```

### Local machine (Spring Boot & ReactJS)
First remove or comment out `messenger_app` and `messenger_ui` from `docker-compose.yml`.
Then run the Docker Compose still for the Bitnami based Kafka images.
```bash
docker-compose build
docker-compose up -d
```

Now start the Spring Boot app locally:
```bash
cd messenger-app
mvn clean install
mvn spring-boot:run
```

Return to the root directory to start the ReactJS app:
```bash
cd ../messenger-client
yarn install
yarn run build
yarn start
```

## Usage

Place a message on the queue
```bash
curl --location --request POST 'localhost:8080/message/' \
--header 'Content-Type: application/json' \
--data-raw '{
	"user": "User 1",
	"data": "Hello world!"
}'
```

The messages will be displayed via the ReactJS app accessible on `http://localhost:3000`

## Outstanding improvements
- Generate new Kafka topics via Spring Boot properties.
- Submit messages via web app instead of cURL.
- Implement Spring Boot web security.
- Further optimise Dockerfile and Docker Compose configuration.