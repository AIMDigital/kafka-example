package com.kamran.messenger;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Value("${spring.kafka.topic.messages}")
	private String messageTopic;


	//TODO generate topics based on properties and check if topic already exists
	@Bean
	public NewTopic messageTopic() {
		return TopicBuilder.name(messageTopic)
				.partitions(1)
				.replicas(1)
				.build();
	}

}
