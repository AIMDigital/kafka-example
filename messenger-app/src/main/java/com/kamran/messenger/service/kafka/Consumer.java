package com.kamran.messenger.service.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class Consumer {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public Consumer(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @KafkaListener(topics = "${spring.kafka.topic.messages}")
    public void receiveMessage(String payload) {
        log.info("Received Kafka payload='{}'", payload);
        simpMessagingTemplate.convertAndSend("/topic/messages", payload);
    }
}
