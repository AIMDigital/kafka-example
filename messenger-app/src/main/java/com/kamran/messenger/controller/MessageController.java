package com.kamran.messenger.controller;


import com.kamran.messenger.model.Message;
import com.kamran.messenger.service.kafka.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(path = "/message")
public class MessageController {

    private Producer producer;

    @Autowired
    public MessageController(Producer producer) {
        this.producer = producer;
    }

    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendMessageToKafka(@RequestBody Message message) {
        log.info("Incoming message: {}", message.toString());
        this.producer.sendMessage(message);
        return ResponseEntity.ok("Processing message");
    }

}