import React, { Component } from "react";

class Message extends Component {

  render() {
    
    if(this.props.message) {
      return (
        <section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
          <div className="mdl-card mdl-cell mdl-cell--12-col">
            <div className="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
              <h4 className="mdl-cell mdl-cell--12-col">{this.props.message.user}</h4>
              <div className="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
                {this.props.message.data}
              </div>
            </div>
          </div>
        </section>
      );
    }
    
  }
  
}

export default Message;