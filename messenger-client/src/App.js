import React, { Component } from 'react';
import SockJsClient from 'react-stomp';
import Header from './components/Header';
import Message from './components/Message';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: []
    };
  }

  addMessage = message =>
    this.setState(prevState => ({ messages: [...prevState.messages, message]}))

  render() {

    const messages = this.state.messages.map(function(message, i) {
      return <Message key={i} message={message} />
    });

    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header has-tabs is-upgraded" data-upgraded=",MaterialLayout">
        <Header />
        <main className="mdl-layout__content">
          <div className="mdl-layout__tab-panel is-active">
            <SockJsClient url='http://localhost:8080/ws' topics={['/topic/messages']}
              onMessage={(msg) => { this.addMessage(msg) }} />
            {messages}
          </div>
          <footer class="mdl-mega-footer"></footer>
        </main>
      </div>
    )
  }
}

export default App;